-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-04-2018 a las 14:59:17
-- Versión del servidor: 10.1.31-MariaDB
-- Versión de PHP: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vg-collection`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `buy`
--

CREATE TABLE `buy` (
  `id_trans_buy` int(11) NOT NULL,
  `id_trans_sell` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `date_buy` date DEFAULT NULL,
  `payed` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `buy`
--

INSERT INTO `buy` (`id_trans_buy`, `id_trans_sell`, `id_user`, `date_buy`, `payed`) VALUES
(7, 1, 2, '2018-04-20', 0),
(8, 1, 2, '2018-04-20', 0),
(9, 1, 2, '2018-04-20', 0),
(10, 1, 2, '2018-04-20', 0),
(11, 1, 2, '2018-04-20', 0),
(12, 1, 2, '2018-04-20', 0),
(13, 1, 2, '2018-04-20', 0),
(14, 1, 2, '2018-04-20', 0),
(15, 1, 2, '2018-04-20', 0),
(16, 1, 2, '2018-04-20', 0),
(17, 1, 2, '2018-04-20', 0),
(18, 1, 2, '2018-04-20', 0),
(19, 1, 2, '2018-04-20', 0),
(20, 1, 2, '2018-04-20', 0),
(21, 1, 2, '2018-04-20', 0),
(22, 1, 2, '2018-04-20', 0),
(23, 1, 2, '2018-04-20', 0),
(24, 1, 2, '2018-04-20', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comments`
--

CREATE TABLE `comments` (
  `id_comment` int(11) NOT NULL,
  `id_game` int(11) NOT NULL,
  `id_platform` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `ranking` enum('1 star','2 stars','3 stars','4 stars','5 stars') NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(500) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consoles`
--

CREATE TABLE `consoles` (
  `id_consoles` int(11) NOT NULL,
  `name_console` varchar(255) CHARACTER SET utf8 NOT NULL,
  `platform` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `consoles`
--

INSERT INTO `consoles` (`id_consoles`, `name_console`, `platform`) VALUES
(21, 'Nintendo Enternainment System', 3),
(22, 'Super Nintendo', 3),
(23, 'Nintendo 64', 3),
(24, 'Game Cube', 3),
(25, 'Wii', 3),
(26, 'Wii u', 3),
(27, 'Nintendo Switch', 3),
(28, 'Game Boy', 3),
(29, 'Game Boy Advance', 3),
(30, 'Nintendo DS', 3),
(31, 'Nintendo 3DS', 3),
(32, 'Playstation ', 4),
(33, 'Playstation 2', 4),
(34, 'Playstation 3', 4),
(35, 'Playstation 4', 4),
(36, 'PSP', 4),
(37, 'PS Vita', 4),
(38, 'XBOX', 5),
(39, 'XBOX 360', 5),
(40, 'XBOX ONE', 5),
(41, 'Windows', 2),
(42, 'MAC', 2),
(43, 'Linux', 2),
(44, 'Master System', 6),
(45, 'Mega Drive', 6),
(46, 'Sega Saturn', 6),
(47, 'Dreamcast', 6),
(48, 'Game Gear', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `favorites`
--

CREATE TABLE `favorites` (
  `id_favorites` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_trans_sell` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `friends`
--

CREATE TABLE `friends` (
  `id_user` int(11) NOT NULL,
  `id_friend` int(11) NOT NULL,
  `accepted` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `games`
--

CREATE TABLE `games` (
  `id_game` int(11) NOT NULL,
  `id_console` int(11) NOT NULL,
  `id_genre` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `compañia` enum('Sunsoft','Konami','Capcom','SquareSoft','Square-Enix','Taito','Hudson Soft','Akklaim','Nintendo','Sega','Jaleco','Enix','Tecmo','Bandai','Namco','Bandai-Namco','Kemko','Koei','NIS','Level 5') NOT NULL,
  `año` year(4) NOT NULL,
  `description` varchar(255) NOT NULL,
  `cover` varchar(1000) NOT NULL,
  `region` enum('NTSC-JAP','NTSC-USA','PAL-ES','PAL-FR','PAL-UK','PAL-IT','PAL-NOE') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `games`
--

INSERT INTO `games` (`id_game`, `id_console`, `id_genre`, `title`, `compañia`, `año`, `description`, `cover`, `region`) VALUES
(1, 21, 4, 'Batman', 'Sunsoft', 1990, 'El primer juego de Batman para NES', '', 'PAL-ES'),
(3, 21, 4, 'Blue Shadow 2', 'Taito', 1990, 'Juego a lo shadow warriors, esta muy bien', '', 'PAL-ES'),
(4, 21, 4, 'Blue Shadow', 'Taito', 1990, 'Juego a lo shadow warriors, esta muy bien', '', 'PAL-ES'),
(5, 21, 4, 'Blue Shadow 3', 'Taito', 1990, 'Juego a lo shadow warriors, esta muy bien', '', 'PAL-ES');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `game_info`
--

CREATE TABLE `game_info` (
  `id_game` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `local_ubication` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `lent` tinyint(4) DEFAULT NULL,
  `finished` tinyint(4) NOT NULL,
  `date_finish` date NOT NULL,
  `annotations` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `unidades` int(11) NOT NULL,
  `selling` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `game_info`
--

INSERT INTO `game_info` (`id_game`, `id_user`, `local_ubication`, `lent`, `finished`, `date_finish`, `annotations`, `unidades`, `selling`) VALUES
(1, 1, 'Estanteria 1, espacio 8', 0, 0, '2018-01-01', 'juegazo', 1, 1),
(5, 2, 'mi casa', 0, 0, '2000-01-01', 'Me ha molado mucho', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `genres`
--

CREATE TABLE `genres` (
  `id_genre` int(11) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `genres`
--

INSERT INTO `genres` (`id_genre`, `type`) VALUES
(1, 'RPG'),
(2, 'Action-RPG'),
(3, 'Lucha'),
(4, 'Plataformas'),
(5, 'Beat \'em up'),
(6, 'Deportes'),
(7, 'Shooters'),
(8, 'Aventura'),
(9, 'Carreras'),
(10, 'Arcade'),
(11, 'Sandbox'),
(12, 'Puzzle'),
(13, 'Survival-Horror'),
(14, 'Estrategia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `messages`
--

CREATE TABLE `messages` (
  `id_message` int(11) NOT NULL,
  `sender` int(11) NOT NULL,
  `receiver` int(11) NOT NULL,
  `message` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `platform`
--

CREATE TABLE `platform` (
  `id_platform` int(11) NOT NULL,
  `name_platform` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `platform`
--

INSERT INTO `platform` (`id_platform`, `name_platform`) VALUES
(1, 'All'),
(5, 'Microsoft'),
(3, 'Nintendo'),
(6, 'Sega'),
(4, 'Sony'),
(2, 'Steam');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sell`
--

CREATE TABLE `sell` (
  `id_trans_sell` int(11) NOT NULL,
  `id_game` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `date_sell` date NOT NULL,
  `price` int(11) NOT NULL,
  `description` varchar(500) CHARACTER SET utf8 NOT NULL,
  `buyed` tinyint(4) NOT NULL,
  `send` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sell`
--

INSERT INTO `sell` (`id_trans_sell`, `id_game`, `id_user`, `date_sell`, `price`, `description`, `buyed`, `send`) VALUES
(1, 1, 1, '2018-04-06', 35, 'juego batman a la venta', 1, 0),
(2, 4, 2, '2018-04-23', 22, 'blue shadow de noreia a la venta', 1, 1),
(3, 4, 2, '2018-04-20', 11, 'puesta a la venta desde postman', 0, 1),
(4, 4, 2, '2018-04-20', 11, 'puesta a la venta desde postman', 0, 1),
(5, 4, 2, '2018-04-20', 11, 'puesta a la venta desde postman', 0, 1),
(6, 5, 2, '2018-04-20', 11, 'puesta a la venta desde postman', 0, 1),
(7, 5, 2, '2018-04-20', 11, 'puesta a la venta desde postman', 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `user` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `lat` int(11) NOT NULL,
  `lng` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id_user`, `user`, `email`, `password`, `name`, `surname`, `avatar`, `lat`, `lng`) VALUES
(1, 'belmont02', 'joseamores007@hotmail.com', 'jose', 'jose', 'amores', '', 38, -1),
(2, 'Noreia', 'vero@vero.com', 'cc491de401e5dbcde41ef91090975f42', 'Verónica', 'Carbonell Bernabeu', 'avatar.jpg', 12, 12);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `buy`
--
ALTER TABLE `buy`
  ADD PRIMARY KEY (`id_trans_buy`),
  ADD KEY `FK_buy_sell` (`id_trans_sell`),
  ADD KEY `FK_buy_user` (`id_user`);

--
-- Indices de la tabla `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id_comment`),
  ADD KEY `FK_comments_user` (`id_user`),
  ADD KEY `FK_comments_platform` (`id_platform`),
  ADD KEY `FK_comments_game` (`id_game`);

--
-- Indices de la tabla `consoles`
--
ALTER TABLE `consoles`
  ADD PRIMARY KEY (`id_consoles`),
  ADD UNIQUE KEY `name_console` (`name_console`),
  ADD KEY `FK_consoles_platform` (`platform`);

--
-- Indices de la tabla `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`id_favorites`),
  ADD KEY `FK_favorites_user` (`id_user`),
  ADD KEY `FK_favorites_trans_sell` (`id_trans_sell`);

--
-- Indices de la tabla `friends`
--
ALTER TABLE `friends`
  ADD PRIMARY KEY (`id_user`,`id_friend`),
  ADD KEY `FK_friend_id_friend_id_user` (`id_friend`);

--
-- Indices de la tabla `games`
--
ALTER TABLE `games`
  ADD PRIMARY KEY (`id_game`,`id_console`,`region`,`title`),
  ADD KEY `FK_games_console` (`id_console`),
  ADD KEY `FK_games_genre` (`id_genre`);

--
-- Indices de la tabla `game_info`
--
ALTER TABLE `game_info`
  ADD PRIMARY KEY (`id_game`,`id_user`),
  ADD KEY `FK_game_info_id_game` (`id_game`),
  ADD KEY `FK_game_info_id_user` (`id_user`);

--
-- Indices de la tabla `genres`
--
ALTER TABLE `genres`
  ADD PRIMARY KEY (`id_genre`);

--
-- Indices de la tabla `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id_message`),
  ADD KEY `FK_sender_user` (`sender`),
  ADD KEY `FK_receiver_user` (`receiver`);

--
-- Indices de la tabla `platform`
--
ALTER TABLE `platform`
  ADD PRIMARY KEY (`id_platform`),
  ADD UNIQUE KEY `name_platform` (`name_platform`);

--
-- Indices de la tabla `sell`
--
ALTER TABLE `sell`
  ADD PRIMARY KEY (`id_trans_sell`,`id_game`,`id_user`),
  ADD KEY `FK_sell_id_game` (`id_game`),
  ADD KEY `FK_sell_id_user` (`id_user`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `user` (`user`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `buy`
--
ALTER TABLE `buy`
  MODIFY `id_trans_buy` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de la tabla `comments`
--
ALTER TABLE `comments`
  MODIFY `id_comment` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consoles`
--
ALTER TABLE `consoles`
  MODIFY `id_consoles` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT de la tabla `favorites`
--
ALTER TABLE `favorites`
  MODIFY `id_favorites` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `games`
--
ALTER TABLE `games`
  MODIFY `id_game` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `genres`
--
ALTER TABLE `genres`
  MODIFY `id_genre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `messages`
--
ALTER TABLE `messages`
  MODIFY `id_message` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `platform`
--
ALTER TABLE `platform`
  MODIFY `id_platform` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `sell`
--
ALTER TABLE `sell`
  MODIFY `id_trans_sell` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `buy`
--
ALTER TABLE `buy`
  ADD CONSTRAINT `FK_buy_sell` FOREIGN KEY (`id_trans_sell`) REFERENCES `sell` (`id_trans_sell`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_buy_user` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `FK_comments_game` FOREIGN KEY (`id_game`) REFERENCES `games` (`id_game`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_comments_platform` FOREIGN KEY (`id_platform`) REFERENCES `platform` (`id_platform`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_comments_user` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `consoles`
--
ALTER TABLE `consoles`
  ADD CONSTRAINT `FK_consoles_platform` FOREIGN KEY (`platform`) REFERENCES `platform` (`id_platform`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `favorites`
--
ALTER TABLE `favorites`
  ADD CONSTRAINT `FK_favorites_trans_sell` FOREIGN KEY (`id_trans_sell`) REFERENCES `sell` (`id_trans_sell`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_favorites_user` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `friends`
--
ALTER TABLE `friends`
  ADD CONSTRAINT `FK_friend_id_friend_id_user` FOREIGN KEY (`id_friend`) REFERENCES `users` (`id_user`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_friend_id_user_id_user` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `games`
--
ALTER TABLE `games`
  ADD CONSTRAINT `FK_games_console` FOREIGN KEY (`id_console`) REFERENCES `consoles` (`id_consoles`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_games_genre` FOREIGN KEY (`id_genre`) REFERENCES `genres` (`id_genre`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `game_info`
--
ALTER TABLE `game_info`
  ADD CONSTRAINT `FK_game_info_id_game` FOREIGN KEY (`id_game`) REFERENCES `games` (`id_game`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_game_info_id_user` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `FK_receiver_user` FOREIGN KEY (`receiver`) REFERENCES `users` (`id_user`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_sender_user` FOREIGN KEY (`sender`) REFERENCES `users` (`id_user`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `sell`
--
ALTER TABLE `sell`
  ADD CONSTRAINT `FK_sell_id_game` FOREIGN KEY (`id_game`) REFERENCES `games` (`id_game`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_sell_id_user` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
