const express = require('express');
const base64Img = require('base64-img');
let AuthUser = require('../models/authUser');
let Game = require('../models/game');
let Comentary = require('../models/comentary');
let router = express.Router();

let fs = require('fs');

router.get('/', (req, res) => {
    Game.getAllGames().then(respuesta => {
        res.send({
            error: false,
            ok: true,
            games: respuesta
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessage: 'Error: ' + error
        });
    });
});

router.get('/platform', (req, res) => {
    Game.getAllPlatforms().then(respuesta => {
        res.send({
            error: false,
            ok: true,
            platforms: respuesta
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessage: 'Error: ' + error
        });
    });
});

router.get('/genres', (req, res) => {
    Game.getAllGenres().then(respuesta => {
        res.send({
            error: false,
            ok: true,
            genres: respuesta
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessage: 'Error: ' + error
        });
    });
});

router.get('/platform/:id', (req, res) => {
    Game.getAllConsolesPerPlatform(req.params.id).then(respuesta => {
        res.send({
            error: false,
            ok: true,
            consoles: respuesta
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessage: 'Error: ' + error
        });
    });
});

router.get('/consoles', (req, res) => {
    Game.getAllConsoles().then(respuesta => {
        res.send({
            error: false,
            ok: true,
            consoles: respuesta
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessage: 'Error: ' + error
        });
    });
});

router.get('/console/:id', (req, res) => {
    Game.getAllGamesConsoles(req.params.id).then(respuesta => {
        res.send({
            error: false,
            ok: true,
            games: respuesta
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessage: 'Error: ' + error
        });
    });
});

router.get('/comments/:id', (req, res) => {
    Comentary.getAllCommentsOneGame(req.params.id).then(respuesta => {
        res.send({
            error: false,
            ok: true,
            comments: respuesta
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessage: 'Error: ' + error
        });
    });
});

router.get('/amount', (req, res) => {
    Game.getAmountGames().then(respuesta => {
        res.send({
            error: false,
            ok: true,
            amount: respuesta[0]
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessage: 'Error: ' + error
        });
    });
});

router.get('/scan/:id', (req, res) => {
    Game.getOneGameByScanBarCode(req.params.id).then(respuesta => {
        console.log(respuesta[0]);
        if (respuesta[0] !== undefined) {
            res.send({
                error: false,
                ok: true,
                game: respuesta[0]
            });
        }
        else {
            res.send({
                error: false,
                    ok: true,
                    game: 'no existe el codigo de barras en nuestra base de datos'
            });
        }
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessage: 'Error: ' + error
        });
    });
});

router.get('/:id', (req, res) => {
    Game.getOneGame(req.params.id).then(respuesta => {
        res.send({
            error: false,
            ok: true,
            game: respuesta[0]
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessage: 'Error: ' + error
        });
    });
});

router.post('/', (req, res) => {
    let game = req.body;
    game.cover = guardarImagen(game.cover);
    Game.AddGame(game).then(respuesta => {
        res.send({
            error: false,
            ok: true,
            game: respuesta
        });

    }).catch(error => {
        res.send({
            error: true,
            ok: false,
            errorMsg: 'Error: ' + error
        });
    });
});

router.post('/comments/:id', (req, res) => {
    let body = [];
    let datos = req.body;
    let newComment = new Comentary(datos);
    let token = req.headers['authorization'];
    let valToken = AuthUser.validarToken(token);
    newComment.AddGameComment(req.params.id, valToken.id).then(respuesta => {
        res.send({
            error: false,
            ok: true,
            game: "id del juego: " + respuesta
        });

    }).catch(error => {
        res.send({
            error: true,
            ok: false,
            errorMsg: 'Error: ' + error
        });
    });
});

router.put('/:id', (req, res) => {
    let datos = req.body;
    datos.cover = guardarImagen(datos.cover);
    let newGame = new Game(datos);
    newGame.updateGame(req.params.id).then(respuesta => {
        res.send({
            ok: true,
            error: false,
            game: respuesta
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessge: 'Error: ' + error
        });
    });
});

router.delete('/:id', (req, res) => {
    Game.deleteGame(req.params.id).then(respuesta => {
        res.send({
            error: false,
            ok: true,
            result: "juego borrados: " + respuesta
        });

    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessge: 'Error: ' + error
        });
    });
});

module.exports = router;

function guardarImagen(image) {
    image = image.replace(/^data:image\/png;base64,/, "");
    image = image.replace(/^data:image\/jpg;base64,/, "");
    image = image.replace(/^data:image\/jpeg;base64,/, "");

    image = Buffer.from(image, 'base64');

    let nameFile = new Date().getTime() + '.jpg';

    fs.writeFileSync('./public/img/covers/' + nameFile, image);

    return nameFile;
}