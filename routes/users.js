const express = require('express');
let AuthUser = require('../models/authUser');
let User = require('../models/user');
let router = express.Router();
let fs = require('fs');

//devuelve un array con todos los usuarios (postman funciona)
router.get('/', (req, res) => {
    let token = req.headers['authorization'];
    let valToken = AuthUser.validarToken(token);
    User.getAllUsers(valToken.id).then(respuesta => {
        res.send({
            error: false,
            ok: true,
            users: respuesta
        });
    }).catch(error => {
        res.send({
            error: true,
            errorMessge: 'Error: ' + error
        });
    });
});

//devuelve el perfil del usuario logueado
router.get('/me', (req, res) => {
    //validando token
    let token = req.headers['authorization'];
    let valToken = AuthUser.validarToken(token);
    //con esto saco el id del token => 
    console.log('/me', valToken.id);
    User.getUserLogged(valToken.id).then(respuesta => {
        console.log(valToken.id);
        res.send({
            error: false,
            ok: true,
            user: respuesta[0],
            token: token
        });
    }).catch(error => {
        res.send({
            error: true,
            errorMessge: 'Error: ' + error
        });
    });
});

//Encuentra a un Usuario por su id
router.get('/:id', (req, res) => {
    //validando token
    let token = req.headers['authorization'];
    let valToken = AuthUser.validarToken(token);
    //con esto saco el id del token => console.log(valToken.id);
    User.FindUserById(req.params.id, valToken.id).then(respuesta => {
        res.send({
            ok: true,
            error: false,
            user: respuesta[0]
        });

    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessge: 'Error: ' + error
        });
    });
});

//Actualiza coordenadas
router.put('/coords', (req, res) => {
    //validando token
    let token = req.headers['authorization'];
    let valToken = AuthUser.validarToken(token);
    let user = new User(req.body);
    user.updateCoords(valToken.id).then(
        respuesta => {
            res.send({
                error: false,
                ok: true,
                user: respuesta,
                token: token
            });
        }).catch(error => {
            res.send({
                error: true,
                errorMessge: 'Error: ' + error
            });
        });
});

//Actualiza user, name, surname y email
router.put('/:id', (req, res) => {
    //validando token
    let token = req.headers['authorization'];
    let valToken = AuthUser.validarToken(token);
    let user = new User(req.body);
    user.updateNameEmail(valToken.id).then(respuesta => {
        res.send({
            error: false,
            ok: true,
            user: respuesta,
            token: token
        });
    }).catch(error => {
        res.send({
            error: true,
            errorMessge: 'Error: ' + error
        });
    });
})

//Actualiza avatar del usuario
router.patch('/me/avatar', (req, res) => {
    //validando token
    
    let user = new User(req.body);
    user.avatar = guardarImagen(user.avatar);

    // if (req.files) {
    //     let date = new Date();
    //     let nameFile = date.getDate() + date.getMilliseconds() + '.jpg';
    //     req.files.avatar.mv('./public/img/users/' + nameFile, error => {
    //         if (error) {
    //             res.status(500).send('Error');
    //         }
    //         user.avatar = nameFile;
    //     });
    // }
    // else { 
    //     user.avatar = 'avatar.jpg';
    // }

    user.updateAvatar(valToken.id).then(respuesta => {
        res.send({
            ok: true,
            error: false,
            user: user,
            result: respuesta
        });

    }).catch(error => {
        res.send({
            error: true,
            errorMessge: 'Error: ' + error
        });
    });

})

//Actualiza el password del usuario
//TODO me pilla el body como vacio --> mirarlo más adelante
router.patch('/me/password', (req, res) => {
    //validando token
    let token = req.headers['authorization'];
    let valToken = AuthUser.validarToken(token);
    let user = new User(req.body);
    
    user.updatePassword(valToken.id).then(respuesta => {
        res.send({
            error: false,
            user: user
        });

    }).catch(error => {
        res.send({
            error: true,
            errorMessge: 'Error: ' + error
        });
    });
})

//Borra un Usuario por su id (postman)
router.delete('/:id', (req, res) => {
    //validando token
    let token = req.headers['authorization'];
    let valToken = AuthUser.validarToken(token);
    //con esto saco el id del token => console.log(valToken.id);
    User.deleteUser(req.params.id).then(respuesta => {
        res.send({
            ok: true,
            error: false,
            user: respuesta[0]
        });

    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessge: 'Error: ' + error
        });
    });
})

module.exports = router;

function guardarImagen(image) {
    image = image.replace(/^data:image\/png;base64,/, "");
    image = image.replace(/^data:image\/jpg;base64,/, "");
    image = image.replace(/^data:image\/jpeg;base64,/, "");

    image = Buffer.from(image, 'base64');

    let nameFile = new Date().getTime() + '.jpg';

    fs.writeFileSync('./public/img/users/' + nameFile, image);

    return nameFile;
}