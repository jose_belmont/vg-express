const express = require('express');

let AuthUser = require('../models/authUser');
let Market = require('../models/market');
let router = express.Router();

let fs = require('fs');

router.get('/', (req, res) => {
    Market.getAllProducts().then(respuesta => {
        res.send({
            error: false,
            ok: true,
            products: respuesta
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessage: 'Error: ' + error
        });
    });
});

router.get('/amount', (req, res) => {   
    Market.getAmountGamesForSell().then(respuesta => {
        res.send({
            error: false,
            ok: true,
            amount: respuesta[0]
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessage: 'Error: ' + error
        });
    });
});

router.get('/my', (req, res) => {
    let token = req.headers['authorization'];
    let valToken = AuthUser.validarToken(token);
    Market.getAllProductsByUser(valToken.id).then(respuesta => {
        res.send({
            error: false,
            ok: true,
            products: respuesta
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessage: 'Error: ' + error
        });
    });
});

router.get('/user/:id', (req, res) => {
    Market.getAllProductsByUser(req.params.id).then(respuesta => {
        res.send({
            error: false,
            ok: true,
            products: respuesta
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessage: 'Error: ' + error
        });
    });
});

router.get('/myselled', (req, res) => {
    let token = req.headers['authorization'];
    let valToken = AuthUser.validarToken(token);
    Market.getMySelledProducts(valToken.id).then(respuesta => {
        res.send({
            error: false,
            ok: true,
            products: respuesta
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessage: 'Error: ' + error
        });
    });
});

router.get('/platform/:id', (req, res) => {
    Market.getAllProductsByPlatform(req.params.id).then(respuesta => {
        res.send({
            error: false,
            ok: true,
            products: respuesta
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessage: 'Error: ' + error
        });
    });
});

router.get('/console/:id', (req, res) => {
    Market.getAllProductsByConsole(req.params.id).then(respuesta => {
        res.send({
            error: false,
            ok: true,
            products: respuesta
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessage: 'Error: ' + error
        });
    });
});

router.get('/:id', (req, res) => {
    Market.getOneProduct(req.params.id).then(respuesta => {
        res.send({
            error: false,
            ok: true,
            products: respuesta[0]
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessage: 'Error: ' + error
        });
    });
});

router.post('/sell/:id', (req, res) => {
    let datos = req.body;
    let newProduct = new Market(datos);
    let token = req.headers['authorization'];
    let valToken = AuthUser.validarToken(token);
    newProduct.sellProduct(valToken.id, req.params.id).then(respuesta => {
        res.send({
            error: false,
            ok: true,
            game: respuesta.insertId
        });
    }).catch(error => {
        res.send({
            error: true,
            ok: false,
            errorMsg: 'Error: ' + error
        });
    });
});

// router.post('/buy/:id', (req, res) => {
//     let token = req.headers['authorization'];
//     let valToken = AuthUser.validarToken(token);
//     let venta = +req.params.id;
//     Market.buyProduct(valToken.id, venta).then(respuesta => {
//         console.log('esto no sale');
//         res.send({
//             error: false,
//             ok: true,
//             game: respuesta
//         });
//     }).catch(error => {
//         console.log('esto no sale');
//         res.send({
//             error: true,
//             ok: false,
//             errorMsg: 'Error: ' + error
//         });
//     });
// });



// router.patch('/pay/:id', (req, res) => {
//     let newProduct = new Market(req.body);
//     let token = req.headers['authorization'];
//     let valToken = AuthUser.validarToken(token);
//     newProduct.payProduct(valToken.id, req.params.id).then(respuesta => {
//         res.send({
//             ok: true,
//             error: false,
//             game: respuesta
//         });
//     }).catch(error => {
//         res.send({
//             ok: false,
//             error: true,
//             errorMessge: 'Error: ' + error
//         });
//     });
// });

router.put('/sell/:id', (req, res) => {
    let newProduct = new Market(req.body);
    newProduct.updateSellProductToBuyStatus(req.params.id).then(respuesta => {
        res.send({
            ok: true,
            error: false,
            game: respuesta
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessge: 'Error: ' + error
        });
    });
});

router.put('/:id', (req, res) => {
    let newProduct = new Market(req.body);
    let token = req.headers['authorization'];
    let valToken = AuthUser.validarToken(token);
    newProduct.updateProduct(valToken.id, req.params.id).then(respuesta => {
        res.send({
            ok: true,
            error: false,
            game: respuesta
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessge: 'Error: ' + error
        });
    });
});

router.delete('/:id', (req, res) => {
    let token = req.headers['authorization'];
    let valToken = AuthUser.validarToken(token);
    Market.deleteProduct(valToken.id, req.params.id).then(respuesta => {
        res.send({
            error: false,
            ok: true,
            result: "producto borrado: " + respuesta
        });

    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessge: 'Error: ' + error
        });
    })
});

module.exports = router;