const express = require('express');
const base64Img = require('base64-img');
let AuthUser = require('../models/authUser');
let MyGame = require('../models/mygame');
let Comentary = require('../models/comentary');
let router = express.Router();

let fs = require('fs');

router.get('/', (req, res) => {
    let token = req.headers['authorization'];
    let valToken = AuthUser.validarToken(token);
    MyGame.getAllGamesOfUser(valToken.id).then(respuesta => {
        res.send({
            error: false,
            ok: true,
            games: respuesta
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessage: 'Error: ' + error
        });
    });
});

router.get('/amount', (req, res) => {
    let token = req.headers['authorization'];
    let valToken = AuthUser.validarToken(token);
    MyGame.getAmountGames(valToken.id).then(respuesta => {
        res.send({
            error: false,
            ok: true,
            amount: respuesta[0]
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessage: 'Error: ' + error
        });
    });
});

router.get('/platforms', (req, res) => {
    let token = req.headers['authorization'];
    let valToken = AuthUser.validarToken(token);
    MyGame.getAllUserPlatforms(valToken.id).then(respuesta => {
        res.send({
            error: false,
            ok: true,
            platforms: respuesta
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessage: 'Error: ' + error
        });
    });
});

router.get('/mycomments', (req, res) => {
    let token = req.headers['authorization'];
    let valToken = AuthUser.validarToken(token);
    Comentary.getMyComments(valToken.id).then(respuesta => {
        res.send({
            error: false,
            ok: true,
            comments: respuesta
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessage: 'Error: ' + error
        });
    });
});

router.get('/consoles/:id', (req, res) => {
    let token = req.headers['authorization'];
    let valToken = AuthUser.validarToken(token);
    MyGame.getAllUserConsolesPerPlatform(valToken.id, req.params.id).then(respuesta => {
        res.send({
            error: false,
            ok: true,
            consoles: respuesta
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessage: 'Error: ' + error
        });
    });
});

router.get('/games/:id', (req, res) => {
    let token = req.headers['authorization'];
    let valToken = AuthUser.validarToken(token);
    MyGame.getAllUserGamesPerConsole(valToken.id, req.params.id).then(respuesta => {
        res.send({
            error: false,
            ok: true,
            games: respuesta
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessage: 'Error: ' + error
        });
    });
});


router.get('/:id', (req, res) => {
    let token = req.headers['authorization'];
    let valToken = AuthUser.validarToken(token);
    MyGame.getGameOfUser(valToken.id, req.params.id).then(respuesta => {
        res.send({
            error: false,
            ok: true,
            game: respuesta[0]
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessage: 'Error: ' + error
        });
    });
});

router.post('/:id', (req, res) => {
    let datos = req.body;
    let newInfo = new MyGame(datos);
    let token = req.headers['authorization'];
    let valToken = AuthUser.validarToken(token);
    newInfo.id_game = req.params.id;
    newInfo.id_user = valToken.id;
    console.log('my game: ', newInfo);
    newInfo.AddInfoGame(valToken.id, req.params.id).then(respuesta => {
        console.log('my game: ', respuesta);
        res.send({
            error: false,
            ok: true,
            game: respuesta
        });

    }).catch(error => {
        res.send({
            error: true,
            ok: false,
            errorMsg: 'Error: ' + error
        });
    });
});

router.post('/comment/:id', (req, res) => {
    let datos = req.body;
    let newComm = new Comentary(datos);
    let token = req.headers['authorization'];
    let valToken = AuthUser.validarToken(token);
    newComm.AddGameComment(req.params.id, valToken.id).then(respuesta => {
        res.send({
            error: false,
            ok: true,
            game: "id del comentario: " + respuesta
        });

    }).catch(error => {
        res.send({
            error: true,
            ok: false,
            errorMsg: 'Error: ' + error
        });
    });
});

router.put('/:id', (req, res) => {
    let newInfo = new MyGame(req.body);
    let token = req.headers['authorization'];
    let valToken = AuthUser.validarToken(token);
    newInfo.updateInfo(valToken.id, req.params.id).then(respuesta => {
        res.send({
            ok: true,
            error: false,
            game: respuesta
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessge: 'Error: ' + error
        });
    });
});

router.put('/cover/:id', (req, res) => {
    let datos = req.body;
    let newInfo = new MyGame(datos);
    newInfo.my_cover = guardarImagen(newInfo.my_cover);
    let token = req.headers['authorization'];
    let valToken = AuthUser.validarToken(token);
    newInfo.updateCover(valToken.id, req.params.id).then(respuesta => {
        res.send({
            ok: true,
            error: false,
            game: respuesta
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessge: 'Error: ' + error
        });
    });
});

router.put('/comment/:id', (req, res) => {
    let newComm = new Comentary(req.body);
    let token = req.headers['authorization'];
    let valToken = AuthUser.validarToken(token);
    newComm.updateComment(req.params.id, valToken.id).then(respuesta => {
        res.send({
            ok: true,
            error: false,
            comment: respuesta
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessge: 'Error: ' + error
        });
    });
});

router.delete('/:id', (req, res) => {
    let token = req.headers['authorization'];
    let valToken = AuthUser.validarToken(token);
    MyGame.deleteMyGame(valToken.id, req.params.id).then(respuesta => {
        res.send({
            error: false,
            ok: true,
            result: "juego borrados: " + respuesta
        });

    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessge: 'Error: ' + error
        });
    })
});

router.delete('/comment/:id', (req, res) => {
    let token = req.headers['authorization'];
    let valToken = AuthUser.validarToken(token);
    Comentary.deleteMyComment(req.params.id, valToken.id).then(respuesta => {
        res.send({
            error: false,
            ok: true,
            result: "comentario borrado: " + respuesta
        });

    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessge: 'Error: ' + error
        });
    })
});

module.exports = router;

function guardarImagen(image) {
    image = image.replace(/^data:image\/png;base64,/, "");
    image = image.replace(/^data:image\/jpg;base64,/, "");
    image = image.replace(/^data:image\/jpeg;base64,/, "");

    image = Buffer.from(image, 'base64');

    let nameFile = new Date().getTime() + '.jpg';

    fs.writeFileSync('./public/img/covers/' + nameFile, image);

    return nameFile;
}