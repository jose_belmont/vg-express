const express = require('express');

let AuthUser = require('../models/authUser');
let Menu = require('../models/menu');
let router = express.Router();

let fs = require('fs');

router.get('/', (req, res) => {
    Menu.getAllImages().then(respuesta => {
        res.send({
            error: false,
            ok: true,
            images: respuesta
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessage: 'Error: ' + error
        });
    });
});

router.get('/:id', (req, res) => {
    Menu.getMenuImages(req.params.id).then(respuesta => {
        res.send({
            error: false,
            ok: true,
            images: respuesta[0]
        });
    }).catch(error => {
        res.send({
            ok: false,
            error: true,
            errorMessage: 'Error: ' + error
        });
    });
});

module.exports = router;