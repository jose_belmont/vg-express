const express = require('express');
const https = require('https');
const passport = require('passport');
const base64Img = require('base64-img');
let User = require('../models/user');
let AuthUser = require('../models/authUser');
let router = express.Router();

let fs = require('fs');

//funcion de guardar imagen de una url
let saveImage = (url, name) => {
    https.request(url)
        .on('response', function (response) {
            let body = ''
            response.setEncoding('binary');
            response.on('data', function (chunk) {
                body += chunk
            }).on('end', function () {
                fs.writeFileSync(
                    './img/users/' + name + ".jpg ", body, 'binary');
            });
        })
        .end();
}

//--------------- routes ----------------//

router.get('/auth/token', (req, res) => {
    let token = req.headers['authorization'];
    AuthUser.validarToken(token);
    res.send({
        ok: true,
        validateID: AuthUser.validarToken(token),
        error: false
    });
});

//añade un usuario google -> mirar si se tiene que hacer con post
router.get('/auth/google', (req, res) => {
    let token = req.headers['authorization'];
    https.request('https://www.googleapis.com/plus/v1/people/me?access_token=' + token)
        .on('response', function (respons) {
            let body = ''
            respons.on('data', function (chunk) {
                body += chunk
            }).on('end', function () {
                let datos = JSON.parse(body);

                User.addGoogleUser(datos).then(respuesta => {
                    res.send({
                        ok: true,
                        token: respuesta,
                    });
                }).catch(error => {
                    res.send({
                        ok: false,
                        errorMessage: error
                    });

                });
                saveImage(datos.image.url, datos.id);
            })
        })
});

//añade un usuario facebook -> mirar si se tiene que hacer con post
router.get('/auth/facebook', (req, res) => {
    let token = request.headers['authorization'];
    https.request('https://graph.facebook.com/v2.11/me?fields=id,name,email,picture&access_token=' + token)
        .on('response', function (respons) {
            let body = ''
            respons.on('data', function (chunk) {
                body += chunk
            }).on('end', function () {
                let data = JSON.parse(body);
                saveImage(data.picture.data.url, data.id);

                User.addFacebookUser(data).then(respuesta => {
                    res.send({
                        ok: true,
                        token: respuesta,
                    });
                }).catch(error => {
                    res.send({
                        ok: false,
                        errorMessage: error
                    });
                });
            });
        })
        .end();
})

//Hace el Login (probado en postman)
router.post('/auth/login', (req, res) => {
    AuthUser.authUser(req.body).then(
        respuesta => res.send({
            error: false,
            ok: true,
            token: respuesta[0]
        }),
        error => res.send({
            ok: false,
            error: error
        })
    );
});

//Registra el usuario (probado en postman) queda ver el tema imagen en el front
router.post('/auth/register', (req, res) => {
    let user = req.body;
    user.avatar = guardarImagen(user.avatar);
    
    console.log(user);

    // if (!req.files && !user.avatar)
    //     res.status(400).send('Avatar is required');
    // else if (req.files) {
    //     let fileName = new Date().getTime() + '.jpg';
    //     req.files.avatar.mv('./public/img/users/' + fileName, error => {
    //         if (error) res.status(500).send('Error uploading file')
    //         user.avatar = 'avatar.jpg';
    //     });
    // } else {
    //     user.avatar = guardarImagen(user.avatar);
    // }
    User.addUser(user).then(
        resultado => res.send({
            ok: true,
            result: resultado
        }),
        error => {
            res.send({
                    ok: false,
                    error: 'Error al insertar:' + error
                })
                .catch(error => {
                    user.avatar = 'avatar.jpg';
                });
        });

    // fs.unlink('./public/img/users/' + user.avatar, () => { });
});

module.exports = router;

function guardarImagen(image) {
    image = image.replace(/^data:image\/png;base64,/, "");
    image = image.replace(/^data:image\/jpg;base64,/, "");
    image = image.replace(/^data:image\/jpeg;base64,/, "");

    image = Buffer.from(image, 'base64');

    let nameFile = new Date().getTime() + '.jpg';

    fs.writeFileSync('./public/img/users/' + nameFile, image);

    return nameFile;
}