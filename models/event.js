const conexion = require('./bd_config');

module.exports = class Event {
    constructor(eventJSON) {
        this.id = eventJSON.idEvent;
        this.creator = {
            id: eventJSON.creator,
            name: eventJSON.name,
            email: eventJSON.email,
            avatar: eventJSON.avatar,
            lat: eventJSON.lat,
            lng: eventJSON.lng
        };
        this.title = eventJSON.title;
        this.description = eventJSON.description;
        this.date = eventJSON.date;
        this.price = eventJSON.price;
        this.lat = eventJSON.lat;
        this.lng = eventJSON.lng;
        this.address = eventJSON.address;
        this.image = eventJSON.image;
        this.numAttend = eventJSON.numAttend;
        // atributos fantasmas
        this.distance = eventJSON.distance;
        this.attend = eventJSON.attend;
        this.mine = eventJSON.mine;

    }
    //funcion de intento para el metodo el campo attend
    //preguntar porque me devuelve undefined;
    static asistir(usuario, evento) {

        conexion.query(
            `SELECT COUNT(*) as numFilas
                        FROM user_attend_event 
                        WHERE user = ${usuario} AND event = ${evento}`,
            (error, resultado, campos) => {
                if (error) {
                    'Error: ' + error;
                } else {
                    if (resultado[0].numFilas > 0) {
                        return '1';
                    } else {
                        return '0';
                    }
                    return;
                }
            }
        );
    }





    //-------paquete GET -----------------//

    //listar todos los eventos. -> funciona
    static listarEventos(login) {
        return new Promise((resolve, reject) => {
            conexion.query(
                `SELECT event.*, user.*,
                haversine(user.lat, user.lng, event.lat, event.lng) 
                as distance, user.lat as latUser, user.lng as lngUser,
                event.lat as latEvent, event.lng as lngEvent               
                FROM event inner join user on event.creator = user.id`,
                (error, resultado, campos) => {

                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.map(
                                event => {
                                    let ev = new Event(event);
                                    ev.creator.lat = resultado[0].latUser;
                                    ev.creator.lng = resultado[0].lngUser;
                                    ev.lat = resultado[0].latEvent;
                                    ev.lng = resultado[0].lngEvent;

                                    ev.mine = ev.creator.id == login.id ? true : false;
                                    //redondeo para sacar los metros
                                    ev.distance = Math.round(ev.distance * 1000) / 1000;
                                    return ev;
                                })
                        );
                    }
                }
            );
        });
    }

    //listar los eventos de un usuario -> funciona
    static listarMisEventos(login) {
        return new Promise((resolve, reject) => {
            conexion.query(
                `SELECT event.*, user.*, haversine(user.lat, user.lng, event.lat, event.lng) 
                as distance
                FROM event inner join user on event.creator = user.id AND event.creator = ${login.id}`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.map(
                                event => {
                                    let ev = new Event(event)
                                    ev.mine = ev.creator.id == login.id ? true : false;
                                    //redondeo para sacar los metros
                                    ev.distance = Math.round(ev.distance * 1000) / 1000;
                                    //ev.attend = (Event.asistir(login.id, ev.id) > 0 ? true : false);
                                    return ev;
                                })
                        );
                    }
                }
            );
        });
    }

    //listar los eventos a los que asistirá el usuario actual -> funciona
    static listarEventosAsistir(login) {
        return new Promise((resolve, reject) => {
            conexion.query(
                `SELECT event.*, haversine(user.lat, user.lng, event.lat, event.lng) as distance FROM event, user WHERE event.idEvent IN (SELECT event FROM user_attend_event WHERE user= ${login.id}) AND user.id=${login.id} GROUP BY event.idEvent;`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.map(
                                event => {
                                    let ev = new Event(event)
                                    ev.mine = ev.creator.id == login.id ? true : false;
                                    //redondeo para sacar los metros
                                    ev.distance = Math.round(ev.distance * 1000) / 1000;
                                    ev.attend = true;

                                    return ev;
                                })
                        );
                    }
                }
            );
        });
    }

    //buscar un evento por id -> funciona
    static buscarEventoPorId(idEvent, login) {
        return new Promise((resolve, reject) => {
            conexion.query(
                `SELECT event.*, user.*,
                haversine(user.lat, user.lng, event.lat, event.lng) 
                as distance, user.lat as latUser, user.lng as lngUser,
                event.lat as latEvent, event.lng as lngEvent               
                FROM event inner join user on event.creator = user.id WHERE event.idEvent = ${idEvent}`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.map(
                                event => {
                                    let ev = new Event(event);
                                    ev.mine = ev.creator.id == login.id ? true : false;
                                    ev.creator.lat = resultado[0].latUser;
                                    ev.creator.lng = resultado[0].lngUser;
                                    ev.lat = resultado[0].latEvent;
                                    ev.lng = resultado[0].lngEvent;
                                    //redondeo para sacar los metros
                                    ev.distance = Math.round(ev.distance * 1000) / 1000;
                                    return ev;
                                })
                        );
                    }
                }
            );
        });
    }

    //-------paquete POST -----------------//

    //crear un evento -> funciona

    crearEvento() {

        return new Promise((resolve, reject) => {
            let data = {
                creator: this.creator.id,
                title: this.title,
                description: this.description,
                date: this.date,
                price: this.price,
                lat: this.lat,
                lng: this.lng,
                address: this.address,
                image: this.image
            };
            conexion.query(
                'INSERT INTO event SET ?',
                data,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {

                        resolve(
                            resultado.insertId
                        );
                    }
                }
            );

        });
    }
    //asiste a un evento -> funciona
    static asistirEvento(event_id,id, tickets) {
        return new Promise((resolve, reject) => {
            let data = {
                user: id,
                event: event_id,
                tickets: tickets
            };
           
            conexion.query(
                `INSERT INTO user_attend_event SET ?`,
                data,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.insertId
                        );
                    }
                }
            );

        });
    }

    //-------paquete PUT -----------------//

    //actualiza un evento
    actualizarEvento(id, creator) {
        return new Promise((resolve, reject) => {
            let data = {
                title: this.title,
                description: this.description,
                date: this.date,
                price: this.price,
                lat: this.lat,
                lng: this.lng,
                address: this.address,
                image: this.image

            };

            conexion.query(
                `UPDATE event SET ? WHERE idEvent = ${id} AND creator = ${creator}`,
                data,
                
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.affectedRows
                        );
                    }
                }
            );
        });
    }
    //-------paquete DELETE -----------------//

    //borrar un evento -> solo se podra borrar si nadie ha comprado entradas
    static borrarEvento(id) {
        return new Promise((resolve, reject) => {
            conexion.query(
                `DELETE FROM event WHERE idEvent = ${id}`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.affectedRows
                        );
                    }
                }
            );
        });
    }
}