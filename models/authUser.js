const conexion = require('./bd_config');
const jwt = require('jsonwebtoken');
const md5 = require('md5');

const secret = 'secret';

module.exports = class AuthUser {
    static authUser(userJSON) {
        return new Promise((resolve, reject) => {
            conexion.query(
                'SELECT id_user,password FROM USERS WHERE email ="' + userJSON.email + '" AND password = "' + md5(userJSON.password) + '"',
                (error, resultado, campos) => {
                    if (error) return reject(error);
                        if (resultado.length < 1 || resultado[0].password != md5(userJSON.password)) {
                            return reject('Usuario o contraseña incorrecto');
                    } else {
                        resolve(
                            resultado.map(res => {
                                let login = {
                                    id: res.id_user
                                };
                                return this.generarToken(login);
                            })
                        );
                    }

                });
        });
    }

    static generarToken(login) {
        let token = jwt.sign(
            login, secret, {
                expiresIn: '6000000 minutes'
            });
        return token;
    }

    static validarToken(token) {
        try {
            let resultado = jwt.verify(token, secret);
            console.log(resultado);
            return resultado;
        } catch (e) {
            return false;
        }
    }
}