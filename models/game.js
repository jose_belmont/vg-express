const conexion = require('./bd_config');

let id;
let genre = {};
let platform = {};
let consoles = {};
module.exports = class Game {
    constructor(gameJSON) {
        //JSON del juego
        this.id_game = gameJSON.id_game;
        this.id_console = gameJSON.id_console;
        this.id_genre = gameJSON.id_genre;
        this.platform = gameJSON.platform;
        this.img_console = gameJSON.img_console;
        this.img_platform = gameJSON.img_platform;
        this.name_console = gameJSON.name_console;
        this.genre = gameJSON.genre;
        this.title = gameJSON.title;
        this.companyia = gameJSON.companyia;
        this.year = gameJSON.year;
        this.description = gameJSON.description;
        this.cover = gameJSON.cover;
        this.region = gameJSON.region;
        this.barcode = gameJSON.barcode;
        this.youtube = gameJSON.youtube;
    }


    //-------paquete GET -----------------//

    //FUNCIONES DE AYUDA

    //lista todas las regiones
    static getAllGenres() {
        return new Promise((resolve, reject) => {
            conexion.query(`SELECT * FROM GENRES`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            this.genre = resultado
                        );
                        return this.genre;
                    }
                });
        });
    }

    //lista todos las regiones
    // TODO se saca por enum (sino me convence hago una tabla nuevo y pongo la funcion aqui)

    //FUNCIONES DE RUTA

    //Cantidad de juegos que posee el usuario

    static getAmountGames() {
        return new Promise((resolve, reject) => {
            conexion.query(`select count(*) as total from GAMES`,
                (error, resultado, campos) => {
                    if (error) return reject(error);
                    else resolve(
                        resultado.map(r => {
                            return r;
                        }));
                });
        });
    }
    
    //lista todos los juegos
    static getAllGames() {
        return new Promise((resolve, reject) => {
            conexion.query(`SELECT GAMES.* , CONSOLES.name_console as consola, 
                            GENRES.type as genero, PLATFORM.name_platform as plataforma
                            FROM GAMES, CONSOLES, GENRES, PLATFORM
                            WHERE 
                            GAMES.id_console = CONSOLES.id_consoles AND
                            GAMES.id_genre = GENRES.id_genre AND
                            CONSOLES.platform = PLATFORM.id_platform
                            `,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.map(
                                games => {
                                    let game = new Game(games);
                                    game.name_console = resultado[0].consola;
                                    game.genre = resultado[0].genero;
                                    game.platform = resultado[0].plataforma;
                                    return game;
                                }
                            )
                        );
                    }
                });
        });
    }

    //lista todas la plataformas de juegos
    //lista todas las plataformas
    static getAllPlatforms() {
        return new Promise((resolve, reject) => {
            conexion.query(`SELECT * FROM PLATFORM`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            this.platform = resultado
                        );
                        return this.platform;
                    }
                });
        });
    }
    //lista todas las consolas
    static getAllConsoles() {
        return new Promise((resolve, reject) => {
            conexion.query(`SELECT CONSOLES.*, PLATFORM.name_platform as plataforma 
                            FROM CONSOLES, PLATFORM WHERE 
                            CONSOLES.platform = PLATFORM.id_platform;`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            this.consoles = resultado
                        );
                        return this.consoles;
                    }
                });
        });
    }

    //lista todas las consolas de una plataforma
    static getAllConsolesPerPlatform(plataforma) {
        return new Promise((resolve, reject) => {
            conexion.query(`SELECT CONSOLES.* , PLATFORM.name_platform as plataforma
                            FROM PLATFORM, CONSOLES
                            WHERE
                            PLATFORM.id_platform = ${plataforma} AND
                            ${plataforma} = CONSOLES.platform`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            this.consoles = resultado
                        );
                        return this.consoles;
                    }
                });
        });
    }
    //lista todas los juegos de una plataforma --> TODO para un futuro uso
    // static getAllGamesPlatform(platform) {
    //     return new Promise((resolve, reject) => {
    //         conexion.query(`SELECT GAMES.* , CONSOLES.name_console as consola, 
    //                         GENRES.type as genero, PLATFORM.name_platform as plataforma
    //                         FROM GAMES, CONSOLES, GENRES, PLATFORM
    //                         WHERE 
    //                         GAMES.id_console = CONSOLES.id_consoles AND
    //                         GAMES.id_genre = GENRES.id_genre AND
    //                         CONSOLES.platform = PLATFORM.id_platform AND
    //                         PLATFORM.id_platform = ${platform}
    //                         `,
    //             (error, resultado, campos) => {
    //                 if (error) {
    //                     return reject(error);
    //                 } else {
    //                     resolve(
    //                         resultado.map(
    //                             games => {
    //                                 let game = new Game(games);
    //                                 game.name_console = resultado[0].consola;
    //                                 game.genre = resultado[0].genero;
    //                                 return game;
    //                             }
    //                         )
    //                     );
    //                 }
    //             });
    //     });
    // }

    //lista todos los juegos de una consola
    // console es una palabra reservada ¡OJO!
    static getAllGamesConsoles(consoles) {
        return new Promise((resolve, reject) => {
            conexion.query(`SELECT GAMES.* , CONSOLES.name_console as consola, 
                            GENRES.type as genero, PLATFORM.name_platform as plataforma
                            FROM GAMES, CONSOLES, GENRES, PLATFORM
                            WHERE 
                            GAMES.id_console = CONSOLES.id_consoles AND
                            GAMES.id_genre = GENRES.id_genre AND
                            CONSOLES.platform = PLATFORM.id_platform AND
                            CONSOLES.id_consoles = ${consoles}
                            `,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.map(
                                games => {
                                    let game = new Game(games);
                                    game.platform = resultado[0].plataforma;
                                    game.name_console = resultado[0].consola;
                                    game.genre = resultado[0].genero;
                                    return game;
                                }
                            )
                        );
                    }
                });
        });
    }

    //Obtiene el juego que coincide con el codigo de barras
    static getOneGameByScanBarCode(barcode) {
        return new Promise((resolve, reject) => {
            conexion.query(`SELECT GAMES.* , CONSOLES.name_console as consola, 
                            GENRES.type as genero, PLATFORM.name_platform as plataforma
                            FROM GAMES, CONSOLES, GENRES, PLATFORM
                            WHERE 
                            GAMES.id_console = CONSOLES.id_consoles AND
                            GAMES.id_genre = GENRES.id_genre AND
                            CONSOLES.platform = PLATFORM.id_platform AND 
                            GAMES.barcode = ${barcode}
                            `,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.map(
                                games => {
                                    let game = new Game(games);
                                    game.platform = resultado[0].plataforma;
                                    game.name_console = resultado[0].consola;
                                    game.genre = resultado[0].genero;
                                    return game;
                                }
                            )
                        );
                    }
                });
        });
    }
    //lista un juego en concreto
    static getOneGame(id) {
        return new Promise((resolve, reject) => {
            conexion.query(`SELECT GAMES.* , CONSOLES.name_console as consola, 
                            GENRES.type as genero, PLATFORM.name_platform as plataforma
                            FROM GAMES, CONSOLES, GENRES, PLATFORM
                            WHERE 
                            GAMES.id_console = CONSOLES.id_consoles AND
                            GAMES.id_genre = GENRES.id_genre AND
                            CONSOLES.platform = PLATFORM.id_platform AND
                            GAMES.id_game = ${id}
                            `,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.map(
                                games => {
                                    let game = new Game(games);
                                    game.platform = resultado[0].plataforma;
                                    game.name_console = resultado[0].consola;
                                    game.genre = resultado[0].genero;
                                    return game;
                                }
                            )
                        );
                    }
                });
        });
    }

    // +++++++++++++++++++++++++++++++++++//

    //-------paquete POST ----------------//

    //Añade un juego
    static AddGame(game) {
        return new Promise((resolve, reject) => {
            let data = {
                id_console: game.id_console,
                id_genre: game.id_genre,
                title: game.title,
                companyia: game.companyia,
                year: game.year,
                description: game.description,
                cover: game.cover,
                region: game.region,
                barcode: game.barcode,
                youtube: game.youtube
            };
            conexion.query(
                'INSERT INTO GAMES SET ?',
                data,
                (error, resultado, campos) => {
                    if (error) {
                        console.log('error al guardar:', error);
                        return reject(error);
                    } else {
                        console.log('juego guardado');
                        resolve(    
                            resultado.insertId
                        );
                    }
                }
            );

        });
    }
    // +++++++++++++++++++++++++++++++++++//
    //-------paquete PUT -----------------//

    //actualiza un evento
    updateGame(id) {
        return new Promise((resolve, reject) => {
            let data = {
                id_console: this.id_console,
                id_genre: this.id_genre,
                title: this.title,
                compañia: this.compañia,
                year: this.year,
                description: this.description,
                cover: this.cover,
                region: this.region,
                barcode: this.barcode,
                youtube: this.youtube
            };

            conexion.query(
                `UPDATE GAMES SET ? WHERE id_game = ${id}`,
                data,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.affectedRows
                        );
                    }
                }
            );
        });
    }
    // +++++++++++++++++++++++++++++++++++//
    //-------paquete DELETE --------------//

    //borrar un juego -> solo se podra borrar si nadie lo tiene
    static deleteGame(id) {
        return new Promise((resolve, reject) => {
            conexion.query(
                `DELETE FROM GAMES WHERE id_game = ${id}`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.affectedRows
                        );
                    }
                }
            );
        });
    }
    // +++++++++++++++++++++++++++++++++++//
};