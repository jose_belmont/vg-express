const conexion = require('./bd_config');

module.exports = class Commentary {
    constructor(comentaryJSON) {
        this.id_comment = comentaryJSON.id_comment;
        this.id_game = comentaryJSON.id_game;
        this.id_user = comentaryJSON.id_user;
        this.user = comentaryJSON.user;
        this.avatar = comentaryJSON.avatar;
        this.ranking = comentaryJSON.ranking;
        this.title = comentaryJSON.title;
        this.description = comentaryJSON.description;
        this.now = new Date();
        this.date = comentaryJSON.date;
        //atributo fantasma
        this.consola = comentaryJSON.consola;
        this.juego = comentaryJSON.juego;
    }
    //-------paquete GET -----------------//

    //devuelve todos los comentarios
    static getAllCommentsOneGame(game) {
        return new Promise((resolve, reject) => {
            conexion.query(`SELECT COMMENTS.title, COMMENTS.description, 
            COMMENTS.date, USERS.user, USERS.avatar,
            GAMES.title as juego
            FROM COMMENTS, GAMES, USERS
            WHERE 
            COMMENTS.id_user = USERS.id_user AND 
            COMMENTS.id_game = GAMES.id_game AND
            GAMES.id_game = ${game}`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.map(
                                comments => {
                                    let comm = new Commentary(comments);
                                    return comm;
                                }
                            )
                        );
                    }
                }
            );
        });
    }

    static getMyComments(user) {
        return new Promise((resolve, reject) => {
            conexion.query(`SELECT COMMENTS.*, USERS.user, USERS.avatar,
            GAMES.title as juego, CONSOLES.name_console as consola
            FROM COMMENTS, GAMES, USERS, CONSOLES
            WHERE 
            GAMES.id_console = CONSOLES.id_consoles AND
            COMMENTS.id_user = USERS.id_user AND
            COMMENTS.id_game = GAMES.id_game AND
            COMMENTS.id_user = ${user}`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {                        
                        resolve(
                            resultado.map(
                                comments => {
                                    let comm = new Commentary(comments);                                 
                                    return comm;
                                }
                            )
                        );
                    }
                }
            );
        });
    }
    // +++++++++++++++++++++++++++++++++++//

    //-------paquete POST ----------------//

    AddGameComment(game, user) {
        return new Promise((resolve, reject) => {
            let data = {
                id_user: user,
                id_game: game,
                title: this.title,
                description: this.description,
                date: this.date
            };
            conexion.query(
                `INSERT INTO COMMENTS SET ?`,
                data,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.insertId
                        );
                    }
                }
            );
        });
    }
    // +++++++++++++++++++++++++++++++++++//

    //-------paquete PUT -----------------//
    
    updateComment(comment, user) {
        return new Promise((resolve, reject) => {
            let data = {
                title: this.title,
                description: this.description
            };
            conexion.query(
                `UPDATE COMMENTS SET ? WHERE id_comment = ${comment} AND id_user = ${user}`,
                data,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.affectedRows
                        );
                    }
                }
            );
        });
    }
    // +++++++++++++++++++++++++++++++++++//

    //-------paquete DELETE --------------//
    
    static deleteMyComment(comment, user) {
        return new Promise((resolve, reject) => {
            conexion.query(
                `DELETE FROM COMMENTS WHERE id_comment = ${comment} AND
                id_user = ${user}`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.affectedRows
                        );
                    }
                }
            );
        });
    }
    // +++++++++++++++++++++++++++++++++++//
};