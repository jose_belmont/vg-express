const conexion = require('./bd_config');

module.exports = class MyGame {
    constructor(mygameJSON) {
        //JSON de la info personal del juego
        this.id_game = mygameJSON.id_game;
        this.id_user = mygameJSON.id_user;
        this.platform = mygameJSON.platform;
        this.name_console = mygameJSON.name_console;
        this.genre = mygameJSON.genre;
        this.title = mygameJSON.title;
        this.companyia = mygameJSON.companyia;
        this.year = mygameJSON.year;
        this.description = mygameJSON.description;
        this.my_cover = mygameJSON.my_cover;
        this.cover = mygameJSON.cover;
        this.region = mygameJSON.region;
        this.local_ubication = mygameJSON.local_ubication;
        this.gamerscore = mygameJSON.gamerscore;
        this.date_finish = mygameJSON.date_finish;
        this.annotations = mygameJSON.annotations;
        this.trophies = mygameJSON.trophies;
        this.box = mygameJSON.box;
        this.manual = mygameJSON.manual;
        this.mybarcode = mygameJSON.mybarcode;
        this.youtube = mygameJSON.youtube;
        //atributo fantastma
        this.mine = mygameJSON.mine;
    }

    //-------paquete GET -----------------//

    //Cantidad de juegos que posee el usuario

    static getAmountGames(id) {
        return new Promise((resolve, reject) => {
            conexion.query(`select count(*) as total from GAME_INFO where id_user = ${id}`,
                (error, resultado, campos) => {
                    if (error) return reject(error);
                    else resolve(
                        resultado.map(r => {
                            return r;
                        }));
                });
        });
    }

    //lista todos los juegos del usuario logueado
    static getAllGamesOfUser(id) {
        return new Promise((resolve, reject) => {
            conexion.query(`SELECT GAMES.* , GAME_INFO.*, CONSOLES.name_console as consola, 
                            GENRES.type as genero, PLATFORM.name_platform as plataforma
                            FROM GAMES, CONSOLES, GENRES, GAME_INFO, PLATFORM
                            WHERE 
                            GAMES.id_console = CONSOLES.id_consoles AND
                            GAMES.id_genre = GENRES.id_genre AND
                            GAMES.id_game = GAME_INFO.id_game AND
                            CONSOLES.platform = PLATFORM.id_platform AND
                            GAME_INFO.id_user = ${id}
                            `,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.map(
                                games => {
                                    let game = new MyGame(games);
                                    game.platform = resultado[0].plataforma
                                    game.name_console = resultado[0].consola;
                                    game.genre = resultado[0].genero;
                                    return game;
                                }
                            )
                        );
                    }
                });
        });
    }

    //lista todas las plataformas que tenga un usuario
    static getAllUserPlatforms(id) {
        return new Promise((resolve, reject) => {
            conexion.query(`SELECT DISTINCT PLATFORM.* 
                            FROM PLATFORM, GAMES, CONSOLES, GAME_INFO 
                            WHERE
                            PLATFORM.id_platform = CONSOLES.platform AND
                            CONSOLES.id_consoles = GAMES.id_console AND
                            GAME_INFO.id_game = GAMES.id_game AND
                            GAME_INFO.id_user = ${id}`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            this.platform = resultado
                        );
                        return this.platform;
                    }
                });
        });
    }

    //lista todas las consolas que tenga un usuario
    static getAllUserConsolesPerPlatform(id, plataforma) {
        return new Promise((resolve, reject) => {
            conexion.query(`SELECT DISTINCT CONSOLES.* , PLATFORM.name_platform as plataforma
                            FROM PLATFORM, GAMES, CONSOLES, GAME_INFO
                            WHERE
                            PLATFORM.id_platform = ${plataforma} AND
                            ${plataforma} = CONSOLES.platform AND
                            GAME_INFO.id_game = GAMES.id_game AND
                            CONSOLES.id_consoles = GAMES.id_console AND
                            GAME_INFO.id_user = ${id};`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            this.name_console = resultado
                        );
                        return this.name_console;
                    }
                });
        });
    }

    //lista todas las consolas que tenga un usuario
    static getAllUserGamesPerConsole(id, consola) {
        return new Promise((resolve, reject) => {
            conexion.query(`SELECT GAMES.* , PLATFORM.name_platform as plataforma,                                   GAME_INFO.my_cover 
                            FROM PLATFORM, GAMES, CONSOLES, GAME_INFO
                            WHERE
                            PLATFORM.id_platform = CONSOLES.platform AND
                            CONSOLES.id_consoles = ${consola} AND
                            GAME_INFO.id_game = GAMES.id_game AND
                            ${consola} = GAMES.id_console AND
                            GAME_INFO.id_user = ${id};`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            this.name_console = resultado
                        );
                        return this.name_console;
                    }
                });
        });
    }

    //obtiene un juego de el usuario con todos sus datos
    static getGameOfUser(id_user, id_juego) {
        return new Promise((resolve, reject) => {
            conexion.query(`SELECT GAMES.* , GAME_INFO.*, CONSOLES.name_console as consola, 
                            GENRES.type as genero, PLATFORM.name_platform as plataforma
                            FROM GAMES, CONSOLES, GENRES, PLATFORM, USERS, GAME_INFO
                            WHERE 
                            GAMES.id_console = CONSOLES.id_consoles AND
                            GAMES.id_genre = GENRES.id_genre AND
                            CONSOLES.platform = PLATFORM.id_platform AND
                            GAMES.id_game = ${id_juego} AND
                            USERS.id_user = ${id_user} AND
                            GAME_INFO.id_user = ${id_user} AND
                            GAME_INFO.id_game = ${id_juego}`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.map(
                                games => {
                                    let game = new MyGame(games);
                                    game.you
                                    game.platform = resultado[0].plataforma
                                    game.name_console = resultado[0].consola;
                                    game.genre = resultado[0].genero;
                                    return game;
                                }
                            )
                        );
                    }
                });
        });
    }

    // +++++++++++++++++++++++++++++++++++//

    //-------paquete POST ----------------//
    
    //Añade info personal a nuestro juego
    AddInfoGame(id_user, id_game) {
        return new Promise((resolve, reject) => {
            let data = {
                id_game: id_game,
                id_user: id_user,
                local_ubication: this.local_ubication,
                gamerscore: this.gamerscore,
                date_finish: this.date_finish,
                annotations: this.annotations,
                my_cover: this.my_cover,
                mybarcode: this.mybarcode
            };
            conexion.query(
                `INSERT INTO GAME_INFO SET ? `,
                data,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.insertId
                        );
                    }
                }
            );

        });
    }

    AddFromNewGame(id_user) {
        return new Promise((resolve, reject) => {
            let data = {
                id_game: this.id_game,
                id_user: id_user,
                local_ubication: this.local_ubication,
                gamerscore: this.gamerscore,
                date_finish: this.date_finish,
                annotations: this.annotations,
                my_cover: this.my_cover,
                mybarcode: this.mybarcode
            };
            console.log('data to mysql: ', data);
            conexion.query(
                `INSERT INTO GAME_INFO SET ? `,
                data,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.insertId
                        );
                    }
                }
            );

        });
    }

    // +++++++++++++++++++++++++++++++++++//

    //-------paquete PUT --------------//

    //Actualiza la info de nuestro juego
    updateInfo(id_user, id_game) {
        return new Promise((resolve, reject) => {
            let data = {
                local_ubication: this.local_ubication,
                gamerscore: this.gamerscore,
                box: this.box,
                manual: this.manual,
                date_finish: this.date_finish,
                annotations: this.annotations,
                mybarcode: this.mybarcode
            };

            conexion.query(
                `UPDATE GAME_INFO SET ? WHERE id_game = ${id_game} AND id_user = ${id_user}`,
                data,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.affectedRows
                        );
                    }
                }
            );
        });
    }

    updateCover(id_user, id_game) {
        return new Promise((resolve, reject) => {
            let data = {
                my_cover: this.my_cover
            };

            conexion.query(
                `UPDATE GAME_INFO SET ? WHERE id_game = ${id_game} AND id_user = ${id_user}`,
                data,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.affectedRows
                        );
                    }
                }
            );
        });
    }

    // +++++++++++++++++++++++++++++++++++//

    //-------paquete DELETE --------------//

    static deleteMyGame(id_user, id_game) {
        return new Promise((resolve, reject) => {
            conexion.query(
                `DELETE FROM GAME_INFO WHERE id_game = ${id_game} AND id_user = ${id_user}`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.affectedRows
                        );
                    }
                }
            );
        });
    }

    // +++++++++++++++++++++++++++++++++++//
};