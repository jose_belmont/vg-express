const conexion = require('./bd_config');

module.exports = class Sell {
    constructor(sellJSON) {
        this.id_trans_sell = sellJSON.id_trans_sell;
        this.id_game = sellJSON.id_game;
        this.id_user = sellJSON.id_user;
        this.cover = sellJSON.cover;
        this.my_cover = sellJSON.mycover;
        this.title = sellJSON.title;
        this.date_sell = sellJSON.date_sell;
        this.date_buy = sellJSON.date_buy;
        this.today = new Date();
        this.price = sellJSON.price;
        this.description = sellJSON.description;
        this.selled = sellJSON.selled;
        this.send = sellJSON.send;
        this.sellLat = sellJSON.sellLat;
        this.sellLng = sellJSON.sellLng;
        //atributo fantasma
        this.user = sellJSON.user;
        this.lat = sellJSON.lat;
        this.lng = sellJSON.lng;
        this.avatar = sellJSON.avatar;
        this.consola = sellJSON.consola;
        this.distance = sellJSON.distance;
    }
    //-------paquete GET -----------------//

    //devuelve la cantidad de productos que tiene el usuario a la venta
    static getAmountGamesForSell() {
        return new Promise((resolve, reject) => {
            conexion.query(`select count(*) as total from SELL where selled = 0`,
                (error, resultado, campos) => {
                    if (error) return reject(error);
                    else resolve(
                        resultado.map(r => {
                            return r;
                        }));
                });
        });
    }

    //devuelve todos los productos a la venta
    //TODO incorporar la función de distancia y hacer un order by desde el
    //usuario que se le pasa por parametro hasta el que vende el producto.
    static getAllProducts() {
        return new Promise((resolve, reject) => {
            conexion.query(`SELECT GAMES.cover, GAMES.title, SELL.* ,
            haversine(USERS.lat, USERS.lng, SELL.sellLat, SELL.sellLng)
            as distance, 
            CONSOLES.name_console as consola, 
            USERS.lat as lat , USERS.lng as lng,
            USERS.user as user, USERS.avatar as avatar
            FROM SELL, GAMES, USERS, CONSOLES 
            WHERE 
            SELL.id_user = USERS.id_user AND 
            SELL.id_game = GAMES.id_game AND    
            GAMES.id_console = CONSOLES.id_CONSOLES AND
            SELL.selled = 0
            ORDER BY distance`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.map(
                                sell => {
                                    let s = new Sell(sell);
                                    return s;
                                }
                            )
                        );
                    }
                }
            );
        });
    }

    static getAllProductsByUser(user) {
        return new Promise((resolve, reject) => {
            conexion.query(`SELECT GAMES.cover, GAMES.title, 
            SELL.*,
            CONSOLES.name_console as consola,
            USERS.lat, USERS.lng,
            USERS.avatar, USERS.user 
            FROM SELL, GAMES, USERS, CONSOLES
            WHERE 
            SELL.id_user = ${user} AND
            SELL.id_user = USERS.id_user AND 
            SELL.id_game = GAMES.id_game AND
            GAMES.id_console = CONSOLES.id_consoles AND
            SELL.selled = 0 
            ORDER BY SELL.date_sell
            `,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.map(
                                sell => {
                                    let s = new Sell(sell);
                                    return s;
                                }
                            )
                        );
                    }
                }
            );
        });
    }

    static getMySelledProducts(user) {
        return new Promise((resolve, reject) => {
            conexion.query(`SELECT GAMES.cover, GAMES.title, 
            SELL.price, SELL.selled, 
            CONSOLES.name_console as consola,
            USERS.lat, USERS.lng 
            FROM SELL, GAMES, USERS, CONSOLES
            WHERE 
            SELL.id_user = ${user} AND
            SELL.id_user = USERS.id_user AND 
            SELL.id_game = GAMES.id_game AND
            GAMES.id_console = CONSOLES.id_consoles AND 
            SELL.selled = 0 
            ORDER BY SELL.sell_date`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.map(
                                sell => {
                                    let s = new Sell(sell);
                                    return s;
                                }
                            )
                        );
                    }
                }
            );
        });
    }

    static getAllProductsByPlatform(platform) {
        return new Promise((resolve, reject) => {
            conexion.query(`SELECT GAMES.cover, GAMES.title, 
            SELL.price, SELL.selled, CONSOLES.name_console as consola,
            USERS.lat, USERS.lng
            FROM SELL, GAMES, USERS, CONSOLES, PLATFORM
            WHERE 
            SELL.id_user = USERS.id_user AND 
            SELL.id_game = GAMES.id_game AND
            GAMES.id_console = CONSOLES.id_consoles AND 
            CONSOLES.platform = PLATFORM.id_platform AND
            PLATFORM.id_platform = ${platform} AND 
            SELL.selled = 0 `,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.map(
                                sell => {
                                    let s = new Sell(sell);
                                    return s;
                                }
                            )
                        );
                    }
                }
            );
        });
    }

    static getAllProductsByConsole(consola) {
        return new Promise((resolve, reject) => {
            conexion.query(`SELECT GAMES.cover, GAMES.title, 
            SELL.price, SELL.selled, CONSOLES.name_console as consola,
            USERS.lat, USERS.lng
            FROM SELL, GAMES, USERS, CONSOLES
            WHERE 
            SELL.id_user = USERS.id_user AND 
            SELL.id_game = GAMES.id_game AND
            GAMES.id_console = CONSOLES.id_consoles AND
            CONSOLES.id_consoles = ${consola} AND 
            SELL.selled = 0 `,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.map(
                                sell => {
                                    let s = new Sell(sell);
                                    return s;
                                }
                            )
                        );
                    }
                }
            );
        });
    }

    static getOneProduct(id_venta) {
        return new Promise((resolve, reject) => {
            conexion.query(`SELECT GAMES.cover, GAMES.title,
            CONSOLES.name_console as consola,
            USERS.avatar, USERS.user,
            USERS.lat, USERS.lng,
            SELL.*,
            haversine(USERS.lat, USERS.lng, SELL.sellLat, SELL.sellLng)
            as distance 
            FROM GAMES, CONSOLES, USERS, SELL
            WHERE 
            SELL.id_trans_sell = ${id_venta} AND
            SELL.id_user = USERS.id_user AND 
            SELL.id_game = GAMES.id_game AND
            GAMES.id_console = CONSOLES.id_consoles AND 
            SELL.selled = 0`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.map(
                                sell => {
                                    let s = new Sell(sell);
                                    return s;
                                }
                            )
                        );
                    }
                });
        });
    }

    // +++++++++++++++++++++++++++++++++++//

    //-------paquete POST ----------------//

    sellProduct(id_user, id_game) {
        return new Promise((resolve, reject) => {
            let data = {
                id_game: id_game,
                id_user: id_user,
                price: this.price,
                description: this.description,
                send: this.send,
                date_sell: new Date(),
                sellLat: this.sellLat,
                sellLng: this.sellLng,
            };

            conexion.query(
                `INSERT INTO SELL SET ?`,
                data,
                (error, resultado, campos) => {
                    console.log(resultado);
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.insertId
                        );
                    }
                }
            );
        });
    }
    // para el futuro --> no usada
    // static buyProduct(id_user, id_venta) {
    //     console.log('buy');
    //     return new Promise((resolve, reject) => {
    //         let data = {
    //             id_trans_sell: id_venta,
    //             id_user: id_user,
    //             date_buy: new Date()
    //         }
    //         conexion.query(`INSERT INTO buy SET ?`, data,
    //             (error, resultado, campos) => {
    //                 if (error) {
    //                     return reject(error);
    //                 } else {
    //                     resolve(
    //                         resultado.insertId
    //                     );
    //                 }
    //             }
    //         );
    //     });
    // }

    // +++++++++++++++++++++++++++++++++++//

    //-------paquete PUT -----------------//
    // futuro uso -> reformado y no se usa
    //pone a pagado y añade a la colección del comprado 
    // payProduct(id_user, id_trans_buy) {
    //     return new Promise((resolve, reject) => {
    //         let data = {
    //             selled: 0
    //         };
    //         conexion.query(
    //             `UPDATE buy SET ? WHERE id_trans_buy = ${id_trans_buy} AND id_user = ${id_user}`, data,
    //             (error, resultado, campos) => {
    //                 if (error) {
    //                     return reject(error);
    //                 } else {
    //                     resolve(
    //                         resultado.affectedRows
    //                     );
    //                 }
    //             });
    //     });
    // }

    updateSellProductToBuyStatus(id_sell) {
        return new Promise((resolve, reject) => {
            let data = {
                selled: this.selled
            };
            conexion.query(
                `UPDATE SELL SET ? WHERE id_trans_sell = ${id_sell}`, data,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado
                        );
                    }
                });
        });
    }

    updateProduct(id_user, id_venta) {
        return new Promise((resolve, reject) => {
            let data = {
                price: this.price,
                description: this.description,
                send: this.send
            }

            conexion.query(
                `UPDATE SELL SET ? 
                WHERE 
                id_trans_sell = ${id_venta} AND
                id_user = ${id_user}
                `,
                data,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.affectedRows
                        );
                    }
                }
            );
        });
    }

    // +++++++++++++++++++++++++++++++++++//

    //-------paquete DELETE --------------//

    static deleteProduct(id_user, id_venta) {
        return new Promise((resolve, reject) => {
            conexion.query(
                `DELETE FROM SELL 
                WHERE 
                id_trans_sell = ${id_venta} AND
                id_user = ${id_user}
                `,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.affectedRows
                        );
                    }
                }
            );
        });
    }

    // +++++++++++++++++++++++++++++++++++//
}