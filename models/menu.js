const conexion = require('./bd_config');

module.exports = class Menu {
    constructor(menuJSON) {
        this.id_menu = menuJSON.id_menu;
        this.image = menuJSON.image;
    }

    //lista todas las regiones
    static getAllImages() {
        return new Promise((resolve, reject) => {
            conexion.query(`SELECT * FROM menu`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            this.image = resultado
                        );
                        return this.image;
                    }
                });
        });
    }

    static getMenuImages(id) {
        return new Promise((resolve, reject) => {
            conexion.query(`SELECT image FROM menu WHERE id_menu = ${id}`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(resultado);
                    }
                });
        });
    }
    static getBdGamesImage() {
        return new Promise((resolve, reject) => {
            conexion.query(`SELECT image FROM menu WHERE id_menu = ${id}`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(resultado);
                    }
                });
        });
    }
    static getMarketImage() {
        return new Promise((resolve, reject) => {
            conexion.query(`SELECT image FROM menu WHERE id_menu = 3`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(resultado);
                    }
                });
        });
    }
};