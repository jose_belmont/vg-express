const conexion = require('./bd_config');
const md5 = require('md5');
const AuthUser = require('./authUser');
const jwt = require('jsonwebtoken');

const secreto = 'secret';

let newUser = {};
module.exports = class User {
    constructor(userJSON) {
        this.id_user = userJSON.id_user;
        this.user = userJSON.user;
        this.name = userJSON.name;
        this.surname = userJSON.surname;
        this.email = userJSON.email;
        this.password = userJSON.password;
        this.avatar = userJSON.avatar;
        this.lat = userJSON.lat;
        this.lng = userJSON.lng;
        this.mine = userJSON.mine;
        this.id_steam = userJSON.id_steam;
    }

    //-------paquete GET -----------------//

    //Sacar todos los usuarios
    static getAllUsers(id) {
        return new Promise((resolve, reject) => {
            conexion.query(
                `SELECT * FROM USERS`,
                (error, resultado, campos) => {
                    if (error){
                        return reject('Error -> Query GetAllUsers:' ,error)
                    }
                    else {
                        resolve(resultado.map(
                            user => {
                                let us = new User(user);
                                us.mine = us.id_user === id ? true : false;
                                return us;
                            }
                        ))
                    }
                }
            )
        });
    }

    //buscar usuario logueado (postman)
    static getUserLogged(id) {
        return new Promise((resolve, reject) => {
            conexion.query(
                `SELECT * FROM USERS WHERE id_user = ${id}`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        console.log('resultado: ', resultado);
                        resolve(
                            resultado.map(
                                usuario => {
                                    let us = new User(usuario);                               
                                    us.mine = true;
                                    return us;
                                }
                            )
                        );
                    }
                }
            );
        });
    }

    //buscar usuario por id (postman)
    static FindUserById(id, login) {
        return new Promise((resolve, reject) => {
            conexion.query(
                `SELECT * FROM USERS WHERE id_user = ${id}`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.map(
                                user => {
                                    let us = new User(user);
                                    us.mine = login === us.id_user ? true : false;
                                    return us;
                                })
                        );
                    }
                }
            );
        });
    }

    //-------paquete POST -----------------//

    //crear un usuario 
    static addUser(usuario) {
        return new Promise((resolve, reject) => {
            if (!usuario.name) return reject('Name is required');
            if (!usuario.email) return reject('Email is required');
            if (!usuario.password) return reject('Password is required');
            if (!usuario.avatar) return reject('Avatar is required');
            let data = {
                //id_user: usuario.id_user,
                user: usuario.user,
                name: usuario.name,
                surname: usuario.surname,
                email: usuario.email,
                password: md5(usuario.password),
                avatar: usuario.avatar,
                lat: usuario.lat,
                lng: usuario.lng,
                id_steam: usuario.id_steam
            };

            conexion.query(
                'INSERT INTO USERS SET ?',
                data,
                (error, resultado, campos) => {
                    if (error) {
                        if (error.sqlState === "23000") return reject('This email is already registered');
                        return reject(error);
                    }
                    if (resultado.affectedRows < 1) return reject('Error saving');
                    else {
                        resolve(
                            resultado.insertId
                        );
                    }
                }
            );
        });
    }
    // si existe usuario de facebook o google
    static userExists(id) {
        return new Promise((resolve, reject) => {
            conexion.query(`SELECT * FROM USERS WHERE id_facebook = ${id} OR id_google = ${id}`, (error, resultado, campos) => {
                if (error) return reject(error);
                if (resultado[0]) {
                    resolve(new User(resultado[0]))
                }
                resolve(false);
            })
        })
    }

    //Crear un usuario por google
    static addGoogleUser(data) {
        let datos = {
            //id: this.id,
            id_google: data.id,
            email: data.emails[0].value,
            name: data.displayName,
            avatar: data.id + '.png'
        }

        return new Promise((resolve, reject) => {
            this.userExists(datos.id_google).then(resultado => {
                if (!resultado) {
                    conexion.query('INSERT INTO USERS set ?', datos, (error, resultado, campos) => {
                        if (error) {
                            return reject(error);
                        }
                        resolve(
                            AuthUser.generarToken({ id: datos.id_google })
                        );
                    });
                } else {
                    conexion.query({
                        sql: "UPDATE USERS SET ? WHERE email = ?",
                        values: [datos, datos.email]
                    }, (error, resultado, campos) => {
                        if (error) {
                            return reject(error);
                        }
                        resolve(
                            AuthUser.generarToken({ id: datos.id_google })
                        );
                    });
                }
            });
        })
    }

    //Crear un usuario por facebook
    static addFacebookUser(data) {
        let datos = {
            //id: this.id,
            id_facebook: data.id,
            email: data.email,
            name: data.name,
            avatar: data.id + '.jpg'
        }
        return new Promise((resolve, reject) => {
            this.userExists(datos.id_facebook).then(resultado => {
                if (!resultado) {
                    conexion.query('INSERT INTO USERS set ?', datos, (error, resultado, campos) => {

                        if (error) {
                            return reject(error => console.log(error));
                        }
                        resolve(

                            AuthUser.generarToken({id: datos.id_facebook})
                        );
                    });
                } else {
                    conexion.query({
                        sql: "UPDATE USERS SET ? WHERE email = ?",
                        values: [datos, datos.email]
                    }, (error, resultado, campos) => {
                        if (error) {
                            return reject(error);
                        }
                        resolve(
                            AuthUser.generarToken({ id: datos.id_facebook })
                        );
                    });
                }
            });
        })
    }

    //-------paquete PUT -----------------//

    //actualizar user, nombre, apellidos y email
    updateNameEmail(id) {
        //console.log(id, user);
        let data = {
            user: this.user,
            name: this.name,
            surname: this.surname,
            email: this.email
        };
        return new Promise((resolve, reject) => {
            conexion.query(
                `UPDATE USERS SET ? WHERE id_user = ${id}`,
                data,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.affectedRows
                        );
                    }
                }
            );
        });
    }

    //actualizar avatar
    updateAvatar(id) {
        let data = {
            avatar: this.avatar
        };
        return new Promise((resolve, reject) => {
            conexion.query(
                `UPDATE USERS SET ? WHERE id_user = ${id}`,
                data,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.affectedRows
                        );
                    }
                }
            );
        });
    }

    //actualización del password
    updatePassword(id) {
        let data = {
            password: md5(this.password)
        };
        console.log(data);
        return new Promise((resolve, reject) => {
            conexion.query(
                `UPDATE USERS SET ? WHERE id_user = ${id}`,
                data,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.affectedRows
                        );
                    }
                }
            );
        });
    }
    //actualización coordenadas
    updateCoords(id) {
        let data = {
            lat: this.lat,
            lng: this.lng,
            // user: this.user,
            // name: this.name,
            // surname: this.surname,
            // email: this.email
        };
        return new Promise((resolve, reject) => {
            conexion.query(
                `UPDATE USERS SET ? WHERE id_user = ${id}`,
                data,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.affectedRows
                        );
                    }
                }
            );
        });
    }
    //-------paquete DELETE -----------------//

    //borrar un evento -> solo se podra borrar si nadie ha comprado entradas
    static deleteUser(id) {
        return new Promise((resolve, reject) => {
            conexion.query(
                `DELETE FROM USERS WHERE id_user = ${id}`,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    } else {
                        resolve(
                            resultado.affectedRows
                        );
                    }
                }
            );
        });
    }
}