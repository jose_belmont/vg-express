const mysql = require('mysql');

let conexion = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'VG-Collection',
    multipleStatements: true
});

module.exports = conexion;