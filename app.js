const express = require('express');
const cors = require('cors');
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const index = require(__dirname +'/routes/index');
const users = require(__dirname +'/routes/users');
const games = require(__dirname +'/routes/games');
const mygames = require(__dirname +'/routes/mygames');
const markets = require(__dirname +'/routes/markets');
const menus = require(__dirname +'/routes/menus');

const secreto = 'secreto';

let app = express();

app.use(fileUpload());
app.use(bodyParser.json({ limit: '150mb' }));
app.use(bodyParser.urlencoded({ limit: '150mb', extended: false }));
app.use(cors());
app.use('/public', express.static('./public/'));

//palabra principal de las rutas
app.use('/', index);
app.use('/users', users);
app.use('/games', games);
app.use('/mygames', mygames);
app.use('/markets', markets);
app.use('/menus', menus);

app.use( (req, res, next) => {
    res.status(404);
    res.send({ ok:false, error: 'uri not found' });
});

app.listen(8080);
